<?php

/*
 * Napi ige (Igenaptár) API.
 * A napi ige lekérése és meglejenítése json formában
 * appok és szolgáltatások számára.
 * Erre a fájlra kell beállítani a crone ütmezőt éjfélkor...
 */


/*
 * Fuggványeket tartalmazó fájl 
 */
include 'eszkozok.php';

/**
 * Loghoz kell
 */
$GetIPAddress = getrealip();

session_start();
if (isset($_SESSION['view'])) {
    $_SESSION['view'] = $_SESSION['view'] + 1;
} else {
    $_SESSION['view'] = 1;
}

$localIP = getHostByName(getHostName());

/**
 * Loghoz kell vége
 */
/**
 * URL -ből dátum bontása
 */
if (!empty($_GET['datum'])) {

    if (validateDate($_GET['datum'])) {
        /*
         *   echo "URL jó dátum";
         */
        $date = $_GET['datum'];
    } else {
        /**
         *    echo "URL nem jó dátum";
         */
        $date = date('Y-m-d');
    }
} else {

    $date = date('Y-m-d');
}
/**
 * URL -ből dátum bontása vége
 */
/**
 * A mai nap dátuma bontva későbbi feldolgozásra
 */
$year = date('Y');
$mount = date('m');
$day = date('d');

/**
 * A mai nap dátuma bontva vége
 */
/**
 * Itt van egy kis logolás...
 */
$datetime = date('Y-m-d H:i:s');
$txt = $datetime . ", IP," . $GetIPAddress . ", Az oldal ," . $_SESSION['view'] . ",  lefutott";
$mylogfile = file_put_contents('logok/crone_logs.log', $txt . PHP_EOL, FILE_APPEND | LOCK_EX);

/**
 * Logolás vége...
 */
/*
 * Igeliturgia
 */
$url = 'http://igenaptar.katolikus.hu/nap/?holnap=' . $date . '';


/*
 * Lekkérek egy tag-et, ami garantáltan a body ban van... ha nincs akkor
 * nem létezik az oldal vagy üres az oldal...
 */

/*
 * Oldal letöltés tartalom ellenőrzés az év végi URL variálasok miatt
 * Aminek a pozitív hozadéka 2016/2017 es váltásnál, hogy folymatos volt tartalomszolgáltatás
 */
$html = file_get_contents($url);
$dom = new DOMDocument;
$dom->loadHTML($html);

$img = $dom->getElementsByTagName('a')->item(0);

if ($img) {

    $url = $url;
} else {

    /**
     * Év váltásnál itt kell módosítani...
     */
    $url = 'http://igenaptar.katolikus.hu/nap/index2016.php?holnap=' . $date . '';
}

/*
 *  Lekkérek egy tag-et vége
 */

/*
 * URL bontása dátum kinyerése
 * 
 */
$urldatum = @explode("=", $url);

$ffdatum = @explode("-", $urldatum[1]);


if (($year = $ffdatum[0]) or ( $mount !== $ffdatum[1]) or ( $day !== $ffdatum[2])) {

    $date = $urldatum[1];
    $year = $ffdatum[0];
    $mount = $ffdatum[1];
    $day = $ffdatum[2];
}

/*
 * URL bontésa dárum kinyerése vége
 * 
 */

/*
 * Napi ige oldal lekkérése kiírása fájlba 
 */

if (!strstr($url, "http://")) {
    $url = "http://" . $_SERVER['SERVER_NAME'] . $url;
}

$handle = @fopen($url, "r");
if ($handle) {

    $file = @fopen("getnapiige.html", "w");
    if ($file) {
        while (true) {
            $data = fgets($handle, 4096);

            fwrite($file, $data);

            if (true == feof($handle)) {
                break;
            }
        }
        fclose($file);
    }


    fclose($handle);
} else {

    print("Nem lehet kapcsolódni a katolikus.hu oldalhoz: Próbáld meg még egyszer! ") . $url;
}

/*
 * Napi ige oldal lekkérése kiírása fájlba vége
 */

/*
 * Napi Szent ha van lekérdezés fájl létrehozás...
 */
$urlsz = 'http://uj.katolikus.hu/szentek.php?h=' . $mount . '&n=' . $day . '';


if (!strstr($urlsz, "http://")) {
    $urlsz = "http://" . $_SERVER['SERVER_NAME'] . $urlsz;
}

$handlesz = @fopen($urlsz, "r");
if ($handlesz) {

    $filesz = @fopen("napiszent.html", "w");
    if ($filesz) {
        while (true) {
            $datasz = fgets($handlesz, 4096);

            fwrite($filesz, $datasz);

            if (true == feof($handlesz)) {
                break;
            }
        }
        fclose($filesz);
    }


    fclose($handlesz);
} else {

    print("Nem lehet kapcsolódni a katolikus.hu oldalhoz: Próbáld meg még egyszer! ") . $urlsz;
}

/*
 * Napi Szent ha van... vége
 */

/*
 * Zsolozsma lekérdezés fájl létrehozás...
 */

/**
 * xml formátum lekérése és mentése feldolgozáshoz
 */
$urlzs = 'http://zsolozsma.katolikus.hu/cgi-bin/l.cgi?qt=pxml&d=' . $day . '&m=' . $mount . '&r=' . $year . '&j=hu&o0=54&o1=38147&o2=120&o3=0&o4=0&o5=2';

/**
 * Kell a dt változat is a json kódban szerepel mindíg az aktuális dáttummal...
 */
$urlzsdt = 'http://zsolozsma.katolikus.hu/cgi-bin/l.cgi?qt=pdt&d=' . $day . '&m=' . $mount . '&r=' . $year . '&j=hu&o0=54&o1=38147&o2=120&o3=0&o4=0&o5=2';


$xml = file_get_contents($urlzs);
file_put_contents('zsolozsma.xml', $xml);


/*
 * Zsolozsma lekérdezés fájl létrehozás vége
 */

/*
 *  Az countline fugvény lekérdezi az adott fájl adott sorának számát 
 *  és a tartalmát a keresési feltétel alapján
 *  
 * @param type $search
 * @param type $sor
 * @param type $file
 * @return type string
 * 
 */

function countline($search, $sor, $file) {

    $line_number = false;

    if ($handle = fopen($file, "r")) {
        $count = 0;
        while (($line = fgets($handle, 4096)) !== FALSE and ! $line_number) {
            $count++;
            $line_number = (strpos($line, $search) !== FALSE) ? $count : $line_number;
        }


        fclose($handle);
    }

    $lines = file($file); //file in to an array
    return $lines[$line_number + $sor]; //line 2
}

/*
 * A countline fugvény vége
 */


/**
 * 
 *                       Adatok feldolgozása!
 * 
 * 
 */
/*
 * Dátum feldolgozása
 */
$datum_line = countline('</b><br><br>', -1, "getnapiige.html");

$gettitle = explode("</b>", $datum_line);

$fdatum = @strip_tags($gettitle[0]);
$title = @strip_tags($gettitle[1]);


/*
 * Zsolozma feldolgozása
 */


if (file_exists('zsolozsma.xml')) {
    $xml = simplexml_load_file('zsolozsma.xml');

    $DayOfYear = strip_tags($xml->CalendarDay->DayOfYear);
    $DayOfWeek = strip_tags($xml->CalendarDay->DayOfWeek);

    $YearLetter = strip_tags($xml->CalendarDay->Celebration->LiturgicalYearLetter);
    $Week = strip_tags($xml->CalendarDay->Celebration->LiturgicalWeek);
    $WeekOfPsalter = strip_tags($xml->CalendarDay->Celebration->LiturgicalWeekOfPsalter);


    /*
     * A vasárnapok miatt
     */
    $html = file_get_contents('zsolozsma.xml');
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($html);
    $finder = new DomXPath($doc);
    $nodex = $finder->query("//*[contains(@class, 'redtitle')]");



    foreach ($xml->CalendarDay->Celebration as $Celebration) {


        /*
         * A vasárnapok miatt
         */
        if (strip_tags($Celebration->StringTitle->span[0]) == "") {
            $titleone = $doc->saveHTML($nodex->item(0));
        } else
            $titleone = strip_tags($Celebration->StringTitle->span[0]);


        $Celebrations[] = array(
            'titleone' => $titleone,
            'name' => strip_tags($Celebration->LiturgicalCelebrationName),
            'season' => strip_tags($Celebration->LiturgicalSeason),
            'titletwo' => strip_tags($Celebration->StringTitle->span[1]),
            'titlethree' => strip_tags($Celebration->StringTitle->span[2]),
            'type' => strip_tags($Celebration->LiturgicalCelebrationType),
            'communia' => strip_tags($Celebration->StringCommunia->span[0]),
            'color' => strip_tags($Celebration->LiturgicalCelebrationColor),
            'celebrationcommunia-0' => strip_tags($Celebration->LiturgicalCelebrationCommunia[0]),
            'celebrationcommunia-1' => strip_tags($Celebration->LiturgicalCelebrationCommunia[1]),
        );
    }
} else {
    exit('Failed to open zsolozsma.xml file');
}


/*
 *  Szentlecke
 */
$szentlecke_line = countline('justify">ALLELUJA<br>', -2, "getnapiige.html");

$olvasmany2 = strip_tags($szentlecke_line);
$olvasmany2 = rtrim($olvasmany2, " \n.");



/*
 *  Olvasmány
 */
$olvasmany_line = countline('VÁLASZOS ZSOLTÁR', -2, "getnapiige.html");
//echo @strip_tags($olvasmany_line) . " olvasmány<br>";
$olvasmany1 = strip_tags($olvasmany_line);
$olvasmany1 = rtrim($olvasmany1, " \n.");


/*
 * Zsoltár I.
 */
if (@countline("SZENTLECKE", -3, "getnapiige.html") !== "") {
    $zsolt_line = @countline("SZENTLECKE", -3, "getnapiige.html");

    $zsolt = strip_tags($zsolt_line);
    $zsolt = rtrim($zsolt, " \n.");
}



if (@countline("br><b>Zsolt", -1, "getnapiige.html") !== "") {
    $zsolt_line = @countline("br><b>Zsolt", -1, "getnapiige.html");

    $zsolt = strip_tags($zsolt_line);
    $zsolt = rtrim($zsolt, " \n.");
} else {
    
}

$zsolttest = explode(" ", $olvasmany2);

if ($zsolttest[0] == "Zsolt") {

    $zsolt = $olvasmany2;
    $olvasmany2 = "";
} else {
    /**
     * Egyelőre semmi
     */
}

/*
 * Zsoltár I. vége
 */



/*
 *  Evangéliumi igehely
 */
if (countline('EGYETEMES', -2, "getnapiige.html") !== "") {
    $evangelium_line = countline('EGYETEMES', -2, "getnapiige.html");

    $evangelium = strip_tags($evangelium_line);
    $evangelium = rtrim($evangelium, " \n.");
}
/*
 *  Evangéliumi igehely vége
 */

/*
 *  Alleluja vers
 */
if (countline('fy">ALLELUJA<br>', 0, "getnapiige.html") !== "") {

    for ($index = 0; $index < 3; $index++) {

        $sor2 = countline('fy">ALLELUJA<br>', $index, "getnapiige.html");

        $pattern = '/^</p>/';
        preg_match($pattern, $sor2, $matches, PREG_OFFSET_CAPTURE, 3);
        print_r($matches);

        if ($matches !== '</p>') {
            $allelujagondol .= strip_tags($sor2);
            $allelujagondolvers = "";
        } else {
            $allelujagondol = "";
        }
    }
    $allelujagondol = rtrim($allelujagondol, " \r\n.");
}
/*
 *  Alleluja vers vége
 */


/*
 * Zsoltár II. 
 */
if (countline('fy">EVANGÉLIUM ELŐTTI VERS<br>', -2, "getnapiige.html") !== "") {

    countline('fy">EVANGÉLIUM ELŐTTI VERS<br>', -2, "getnapiige.html");
    $zsolt_line2 = countline('fy">EVANGÉLIUM ELŐTTI VERS<br>', -2, "getnapiige.html");
    //echo @strip_tags($zsolt_line) . "<br>";
    $zsolt2 = strip_tags($zsolt_line2);
    $zsolt2 = rtrim($zsolt2, " \n.");

    if ($zsolt2 == $zsolt) {

        $zsolt2 = "";
    }
}

/*
 * Zsoltár II. vége
 */

/*
 *  EVANGÉLIUM ELŐTTI VERS nagyböjt
 */
if (countline('fy">EVANGÉLIUM ELŐTTI VERS<br>', 0, "getnapiige.html") !== "") {

    for ($index3 = 0; $index3 < 3; $index3++) {

        $sor3 = countline('fy">EVANGÉLIUM ELŐTTI VERS<br>', $index3, "getnapiige.html");

        $patter3 = '/^</p>/';
        preg_match($patter3, $sor3, $matches3, PREG_OFFSET_CAPTURE, 3);


        if ($matches3 !== '</p>') {
            $evangeottivers .= strip_tags($sor3);
        } else {
            $evangeottivers = "";
        }
    }

    $allelujagondolvers = rtrim($evangeottivers, " \r\n.");
}
/*
 *  EVANGÉLIUM ELŐTTI VERS nagyböjt vége
 */





/*
 *  Evangélium idézet
 */
if (countline('evangelium', +2, "getnapiige.html") !== "") {
    $evangelium_idezet = countline('evangelium', +2, "getnapiige.html");

    $evangelium_idezet = explode("<i>", $evangelium_idezet);


    if (!empty($evangelium_idezet[1])) {

        $ev_idezet = $evangelium_idezet[1];
    } else {

        if (countline('evangelium', +3, "getnapiige.html") !== "") {
            $evangelium_idezet = countline('evangelium', +3, "getnapiige.html");

            $evangelium_idezet = explode("<i>", $evangelium_idezet);


            if (!empty($evangelium_idezet[1])) {

                $ev_idezet = $evangelium_idezet[1];
            } else {
                if (countline('evangelium', +4, "getnapiige.html") !== "") {
                    $evangelium_idezet = countline('evangelium', +4, "getnapiige.html");

                    $evangelium_idezet = explode("<i>", $evangelium_idezet);


                    if (!empty($evangelium_idezet[1])) {

                        $ev_idezet = $evangelium_idezet[1];
                    } else {
                        $ev_idezet = "";
                    }
                }
            }
        }
    }

    $ev_idezet = strip_tags($ev_idezet);
}
/*
 *  Evangélium idézet vége
 */

/*
 * Változók megtisztítása
 */
if (strpos($allelujagondolvers, 'www') !== false) {
    $allelujagondolvers = "";
}


if (strpos($allelujagondol, 'www') !== false) {
    $allelujagondol = "";
}
/*
 * Változók megtisztítása vége
 */

/*
 * Napi Szent feldolgozása
 */
$szent_line = countline('<table align=left', 0, "napiszent.html");

$szent = strip_tags($szent_line);
$szent = rtrim($szent, " \n.");

/*
 * Napi Szent vége
 */


/*
 * Tömb létrhozása
 */
$strings = array(
    'date' => array(
        'fulldate' => $date,
        'year' => $year,
        'mount' => $mount,
        'day' => $day,
        'dayofweek' => trim($DayOfWeek),
        'week' => trim($Week),
        'dayofyear' => trim($DayOfYear),
        'weekofpsalter' => trim($WeekOfPsalter),
        'yearyetter' => trim($YearLetter)),
    'liturgy' => array(
        'title' => $title,
        'readingone' => $olvasmany1,
        'readingtwo' => $olvasmany2,
        'psalm' => $zsolt . $zsolt2,
        'gospel' => trim($evangelium),
        'gospel_acclamation' => $ev_idezet,
        'alleluja' => $allelujagondol . "" . $allelujagondolvers),
    'dailysaint' => array(
        'name' => trim($szent),
        'sort_url' => 'www.uj.katolikus.hu/szentek.php',
        'url' => $urlsz),
    'breviary' => $Celebrations,
    'source' => array(
        'sort_liturgy_url' => 'wwww.igenaptar.katolikus.hu',
        'liturgy_url' => $url,
        'breviary_url' => $urlzsdt
    )
);


/**
 * Json kód létrehozása  
 * echo (strip_tags(json_encode($strings, JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)));
 */
$fjson = (strip_tags(json_encode($strings, JSON_FORCE_OBJECT)));


if (!empty($_GET['datum'])) {

    if (validateDate($_GET['datum'])) {
//        echo "URL jó dátum";
        echo (strip_tags(json_encode($strings, JSON_FORCE_OBJECT)));
    } else {

        /**
         * echo "URL nem jó dátum";
         */
    }
} else {
/**
 *     echo "Mai jó dátum";
 */

    $current_time = date("H:i:s");
    $sunrise = "00:01:00";
    $sunset = "00:11:00";
    $date1 = DateTime::createFromFormat('H:i:s', $current_time);
    $date2 = DateTime::createFromFormat('H:i:s', $sunrise);
    $date3 = DateTime::createFromFormat('H:i:s', $sunset);
    if ($date1 > $date2 && $date1 < $date3) {

        /**
         * A mai nap létrehozása
         */
        $jmyfile = fopen("napiige.json", "w") or die("Unable to open file!");
        fwrite($jmyfile, $fjson);
    }
}


if (!empty($_GET['datum'])) {

    if (validateDate($_GET['datum'])) {
        $date = $_GET['datum'];
    } else {
        echo "URL nem jó dátum";
        /**
         * 
         */
        if ($localIP == '172.16.15.15') {
            header("Location: http://localhost/NapiIge/api/json/hibaoldal.html"); /* Redirect browser */
        } else {
            header("Location: http://".$_SERVER['SERVER_NAME']."/api/json/hibaoldal.html"); /* Redirect browser */
        }
        $date = date('Y-m-d');
    }
} else {
    $date = date('Y-m-d');
    /**
     * 
     */
    if ($localIP == '172.16.15.15') {
        header("Location: http://localhost/NapiIge/api/json/napiige.json"); /* Redirect browser */
    } else {
        header("Location: http://".$_SERVER['SERVER_NAME']."/api/json/napiige.json"); /* Redirect browser */
    }
    $date = date('Y-m-d');
}

fclose($jmyfile);
?>
