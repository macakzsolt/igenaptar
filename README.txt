Igenaptár - A Szentmise olvasmányai és a napi zsolozsma

API - célja és használat

Ez az API sok sebből is vérzik. Egyelőre nem találtam megoldást arra, 
hogy ezeket az adatokat valamilyen egységes felületről vagy helyről szerezzem be. 
A jövőbeli és a már meglévő alkalmazások létrehozásához egy egységes felületet szeretnék biztosítani. 
PHP, Android és Windows projekteken is dolgozom, ebben a témában, 
csak mert szakmailag érdekel. A nem SQL alapú adatok begyűjtése, feldolgozása, rendszerezése és megjelenítése volt a célom.
Ez nem egy professzionális szolgáltatás, alapvetően saját szándékok megvalósítására készült, de ha felkeltette érdeklődésedet, akkor nyugodtan használd.
Építsd be honlapodba vagy kérd le az adatokat az alkalmazásodból.

