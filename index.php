<!DOCTYPE html>
<html dir="ltr" lang="hu-HU">

    <head>
        <meta charset="utf-8">
        <title>Szentmise olvasmányai, zsolozsma és a napi szent</title>
        <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="style.css" media="screen">
        <!--[if lte IE 7]><link rel="stylesheet" href="style.ie7.css" media="screen" /><![endif]-->
        <link rel="stylesheet" href="style.responsive.css" media="all">


        <script src="jquery.js"></script>
        <script src="script.js"></script>
        <script src="script.responsive.js"></script>
        <meta name="keywords" content="NapiIge ima liturgia">



        <style>.content .postcontent-0 .layout-item-0 { padding-right: 10px;padding-left: 10px;  }
            .ie7 .post .layout-cell {border:none !important; padding:0 !important; }
            .ie6 .post .layout-cell {border:none !important; padding:0 !important; }

        </style>


        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {

                var date = $('#datepicker').datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeYear: true,
                    changeMonth: true,
                   // yearRange: "2016:2017",
                    minDate: new Date(2016, 10, 27),
                    // Date(yyyy, mm - 1, dd)
                    maxDate: new Date(2017, 12, 02),
                    showMonthAfterYear: true,
                    showOn: "button",
//                    buttonImage: "http://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
                    buttonImage: "images/calendar.gif",
                    buttonImageOnly: true,
                    showButtonPanel: true,
                    closeText: "OK",
                    beforeShow: function (input, datepicker) {
                        setTimeout(function () {
                            datepicker.dpDiv.find('.ui-datepicker-current')
                                    .text('Ma')
                                    .click(function () {
                                        $(input)
                                                .datepicker('setDate', new Date())
                                                .datepicker('hide');
                                    });

                        }, 1);
                        return {};
                    },
                    onClose: function (dateText, inst) {

                        window.location.href = "" + this.value;
                        $('#sele_date').datepicker('setDate', new Date());
                        $('#sele_date').submit();
                    },
                }).val();
                console.log(date, "Hello, world!");
                $('#datepicker').datepicker().on("input change", function (e) {
                    console.log("Date changed: ", e.target.value);
                    var dateyes = e.target.value;
                });
                $('#datepicker').change(function () {


                    var myDate = new Date("" + this.value);

                    if (myDate == "Invalid Date") {
                        

                        window.location.href = "";
                        alert(myDate + "Nem megfelelő a dátum formátuma!");
                    } else
     
                        window.location.href = "" + this.value;

                });
            });
            $(document).ready(function ()
            {
                $("img[class='ui-datepicker-trigger']").each(function ()
                {
                    $(this).attr('style', 'position:relative; top:3px; left:3px;');
                });
            });



            $.datepicker.regional['hu'] = {
                closeText: "Bezár",
                prevText: "vissza",
                nextText: "előre",
                currentText: "Ma",
                monthNames: ["Január", "Február", "Március", "Április", "Május", "Június",
                    "Július", "Augusztus", "Szeptember", "Október", "November", "December"],
                monthNamesShort: ["Jan", "Feb", "Már", "Ápr", "Máj", "Jún",
                    "Júl", "Aug", "Szep", "Okt", "Nov", "Dec"],
                dayNames: ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"],
                dayNamesShort: ["Vas", "Hét", "Ked", "Sze", "Csü", "Pén", "Szo"],
                dayNamesMin: ["V", "H", "K", "Sze", "Cs", "P", "Szo"],
                weekHeader: "Hét",
                dateFormat: "yy-mm-dd",
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: ""};

            $.datepicker.setDefaults($.datepicker.regional['hu']);






        </script>


        <script type="text/javascript">

            String.prototype.isValidDate = function () {
                var IsoDateRe = new RegExp("^([0-9]{4})-([0-9]{2})-([0-9]{2})$");
                var matches = IsoDateRe.exec(this);
                if (!matches)
                    return false;


                var composedDate = new Date(matches[1], (matches[2] – 1), matches[3]);
                        return ((composedDate.getMonth() == (matches[2] – 1)) &&
                                (composedDate.getDate() == matches[3]) &&
                                (composedDate.getFullYear() == matches[1])
                                );
            }




        </script>





        <?php
        include 'eszkozok.php';

        if (!empty($_GET['datum'])) {

            if (validateDate($_GET['datum'])) {
                $date = $_GET['datum'];

                //     echo "Jó dátum " . $date;

                $url = 'http://'.$_SERVER['SERVER_NAME'].'/api/json/' . $date . '';
            } else {
//                echo "URL nem jó dátum";
                if ($localIP == '172.16.15.15') { // Egyelőre benne hagyom de a sorsa megpecsételődött...
                    header("Location: http://localhost/NapiIge/api/json/hibaoldal.html"); /* Redirect browser */
                } else {
                    header("Location: http://".$_SERVER['SERVER_NAME']."/api/json/hibaoldal.html"); /* Redirect browser */
                }
                $date = date('Y-m-d');
                $url = 'http://'.$_SERVER['SERVER_NAME'].'/api/json/napiige.json';
                echo "<script>alert('Nem megfelelő a dátum formátuma!'); window.location.href = 'http://".$_SERVER['SERVER_NAME']."';</script>";
            }
        } else {
            $date = date('Y-m-d');
            $url = 'http://'.$_SERVER['SERVER_NAME'].'/api/json/napiige.json';
        }


        $content = file_get_contents($url);
        $json = json_decode($content, true);
        ?>

        <style>

            .button {
                background-color: #4CAF50;  Green 
                position:absolute;
                transition: .5s ease;
                top: 50%;
                left: 25%;
                /*padding-left: 15px;*/
            }

        </style>


    </head>
    <body>
        <div id="main">
            <div class="sheet clearfix">
                <header class="header">

                    <div class="shapes">

                    </div>

                    <h1 class="headline">
                        <a href="http://<?php echo $_SERVER['SERVER_NAME']?>">Napi Ige</a>
                    </h1>
                    <h2 class="slogan">A Szentmise olvasmányai és a napi zsolozsma</h2>





                    <nav class="nav">
                        <ul class="hmenu"><li><a href="http://<?php echo $_SERVER['SERVER_NAME']?>" class="active">Igenaptár</a></li><li><a href="fejlesztoknek.php">API</a></li></ul> 
                    </nav>


                </header>
                <div class="layout-wrapper">
                    <div class="content-layout">
                        <div class="content-layout-row">
                            <div class="layout-cell sidebar1">
                                <div class="block clearfix">
                                    <div class="blockheader">
                                        <h3 class="t">Dátum</h3>
                                    </div>
                                    <div class="blockcontent">
                                        <p>
                                            <input size=15px id="datepicker"  maxlength="100" tabindex="0" name="datum" value="<?php echo $date ?>">

                                        <form action="http://<?php echo $_SERVER['SERVER_NAME']?>">
                                            <input class=button type="submit" value="Ma" />
                                        </form>

                                        </p>

                                        <div id="textDiv"></div>
                                        <script type="text/javascript">

                                        </script>
                                    </div>
                                </div>
                                <div class="block clearfix">
                                    <div class="blockheader">
                                        <h3 class="t">Naptár</h3>
                                    </div>
                                    <div style="color:rgb(102, 102, 102); text-align: left; padding-left: 10px" class="blockcontent" >
                                        <p>
                                            <?php
                                            if (($json['breviary'][0]['type'] !== "") and ( $json['breviary'][0]['type'] !== "féria")) {
                                                print ("<h5>" . $json['breviary'][0]['type'] . "</h5>");
                                            }

                                            if ($json['breviary'][0]['season'] !== "") {
                                                print ("<h5>" . $json['breviary'][0]['season'] . "</h5>");
                                            }

                                            if ($json['date']['weekofpsalter'] !== "") {
                                                print ("<h5>" . $json['date']['weekofpsalter'] . ". zsoltáros hét</h5>");
                                            }

                                            if ($json['date']['yearyetter'] !== "") {
                                                print ("<h5>" . $json['date']['yearyetter'] . " év</h5>");
                                            }

                                            if ($json['date']['dayofyear'] !== "") {
                                                print ("<h5>" . $json['date']['dayofyear'] . ". nap</h5>");
                                            }
                                            ?>

                                        </p>
                                    </div>
                                </div>

                            </div>

                            <div class="layout-cell content"><article class="post article">
                                    <h2 class="postheader">Szentmise olvasmányai</h2>
                                    <div id="textDiv"></div>
                                    <div class="postcontent postcontent-0 clearfix">
                                        <div class="content-layout">
                                            <div class="content-layout-row">
                                                <div class="layout-cell layout-item-0" style="width: 100%" >
                                                    <p></p>

                                                    <?php
                                                    print ("<p><h3>" . $json['liturgy']['title'] . "</h3></p>");



                                                    if ($json['liturgy']['readingone'] !== "") {
                                                        print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingone'] . "</a></p>");
                                                    }

                                                    if ($json['liturgy']['readingtwo'] !== "") {
                                                        print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingtwo'] . "</a></p>");
                                                    }

                                                    if ($json['liturgy']['psalm'] !== "") {
                                                        print ("<p><a href='" . $json['source']['liturgy_url'] . "#zsoltar'>" . $json['liturgy']['psalm'] . "</a></p>");
                                                    }

                                                    if ($json['liturgy']['gospel'] !== "") {
                                                        if ($json['liturgy']['gospel_acclamation'] !== "") {
                                                            $gospel_acclamation = "</br>" . $json['liturgy']['gospel_acclamation'];
                                                        } else
                                                            $gospel_acclamation = "";

                                                        if ($json['liturgy']['alleluja'] !== "") {
                                                            $alleluja = "</br>" . $json['liturgy']['alleluja'] . "";
                                                        } else {
                                                            $alleluja = "";
                                                        }

                                                        print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>" . $json['liturgy']['gospel'] . "</a>" . $gospel_acclamation . str_replace(" – ", " - ", $alleluja) . "</p>");
                                                    }
                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-layout">
                                            <div class="content-layout-row">
                                                <div class="layout-cell layout-item-0" style="width: 50%" >

                                                    <h3><b>Zsolozsma</b></h3>
                                                    <?php
                                                    $breviary_url = $json['source']["breviary_url"];

                                                    if ($json['breviary'][0]['titleone'] !== "") {

                                                        $communia = "";
                                                        $color = "";
                                                        echo "<ul>";
                                                        for ($i = 0; $i < count($json['breviary']); $i++) {

                                                            if ($json['breviary'][$i]["titleone"] !== "") {

                                                                $titleone = $json['breviary'][$i]["titleone"];
                                                            }

                                                            if ($json['breviary'][$i]["communia"] !== "") {

                                                                $communia = $json['breviary'][$i]["communia"] . "</BR>";
                                                            }

                                                            if ($json['breviary'][$i]["titletwo"] !== "") {

                                                                $titletwo = $json['breviary'][$i]["titletwo"] . "</BR>";
                                                            }
                                                            if (($i !== 0) and ( $json['breviary'][$i]['color'] !== "")) {

                                                                $color = "Szín: " . $json['breviary'][$i]['color'] . "</BR>";
                                                            }



                                                            echo "<li><a href='" . $breviary_url . "'>" . $titleone . "</a></BR>"
                                                            . $communia
                                                            . $titletwo
                                                            . $color
                                                            . "</li>";
                                                        }
                                                        echo "</ul>";
                                                    } else
                                                        echo $json['breviary']['titleone'];
                                                    ?>

                                                </div>
                                                <?php
                                                /*
                                                 * Array("zöld", "fehér", "piros", "lila", "rózsaszín", "fekete");
                                                 * lila|fekete, rózsaszín|lila
                                                 */
                                                if ($json['breviary'][0]['color'] !== "") {

                                                    if ($json['breviary'][0]['color'] == "lila") {
                                                        $background_color = "rgb(153, 51, 153)";
                                                        $fcolor = "rgb(255, 255, 255)";
                                                    }

                                                    if ($json['breviary'][0]['color'] == "piros") {
                                                        $background_color = "rgb(255, 0, 0)";
                                                        $fcolor = "rgb(255, 255, 255)";
                                                    }

                                                    if ($json['breviary'][0]['color'] == "zöld") {
                                                        $background_color = "rgb(0, 153, 0)";
                                                        $fcolor = "rgb(255, 255, 255)";
                                                    }

                                                    if ($json['breviary'][0]['color'] == "rózsaszín|lila") {
                                                        $background_color = "rgb(204, 102, 204)";
                                                        $fcolor = "rgb(255, 255, 255)";
                                                    }

                                                    if ($json['breviary'][0]['color'] == "lila|fekete") {
                                                        $background_color = "rgb(0, 0, 0)";
                                                        $fcolor = "rgb(255, 255, 255)";
                                                    }
                                                }
                                                ?>
                                                <div class="layout-cell layout-item-0" style="width: 50%" >
                                                    <blockquote style="margin: 10px 0;color: <?php echo $fcolor ?>; background-color: <?php echo $background_color ?>"><b>A liturgia szine: 

                                                            <?php
                                                            if ($json['breviary'][0]['color'] !== "") {

                                                                $color = "" . $json['breviary'][0]['color'] . "";
                                                                echo "" . $color . "</b>";
                                                            }
                                                            ?>
                                                    </blockquote>
                                                    <?php if ($json['dailysaint']['name'] !== "") { ?>
                                                        <blockquote style="margin: 10px 0;">
                                                            <b>A nap szentje - emléknap</b></br> 
                                                            <?php
                                                            $dailysaint = "<a style='text-decoration: underline;' href='" . $json['dailysaint']['url'] . "'><b>" . $json['dailysaint']['name'] . "</b></a></br>";
                                                            echo $dailysaint;
                                                            ?>
                                                        </blockquote>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </article></div>
                        </div>
                    </div>
                </div>
                <footer class="footer">

                    <div class="">
                        <div class="">
                            <h5 class="">Adatokat szolgáltató forrás oldalak</h5>
                        </div>
                        <div class="">

                            <a href="http://igenaptar.katolikus.hu/"><h5>Igenaptár</h5></a> <a href="http://zsolozsma.katolikus.hu/"><h5>Zsolozsma</h5></a> <a href="http://szentiras.hu/"><h5>Szentiras.hu</h5></a>

                        </div>
                    </div>
                    <div class=" ">
                        <div class="">
                            <h5 class="">Kapcsolódó oldalak</h5>
                        </div>
                        <div class="">

                            <a href="http://evangelium.katolikus.hu/napiev-teljes.php"><h5>@evangelium</h5></a>
                            <a href="http://igenaptar.osb.hu/<?php echo $bdate = date('Ymd'); ?>.html"><h5>Bencés igenaptár</h5></a>
                        </div>
                    </div>
                    <!--<p>Copyright © 2016.</p>-->
                </footer>

            </div>
        </div>
        <script>
            var igemutato = {config: {forditas: 'KNB', tipW: '600px', showNumbers: 'true'}},
            s = document.getElementsByTagName('script')[0],
                    e = document.createElement('script');
            e.id = 'igemutato-script';
            e.src = 'http://molnarm.github.io/igemutato.min.js';
            s.parentNode.insertBefore(e, s);
        </script>

    </body>
</html>