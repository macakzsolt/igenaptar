<?php

/*
 * Napi ige (Igenaptár).
 */



/*
 * getUserIP() és getrealip() IP lekérése a látogatótól
 * 
 * Example:
 * $user_ip = getUserIP();
 * echo $user_ip; 
 * 
 * $MyipAddress = getrealip();
 * echo $MyipAddress; 
 * 
 */

function getUserIP() {
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}

function getrealip() {
    if (isset($_SERVER)) {
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            if (strpos($ip, ",")) {
                $exp_ip = explode(",", $ip);
                $ip = $exp_ip[0];
            }
        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
    } else {
        if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
            if (strpos($ip, ",")) {
                $exp_ip = explode(",", $ip);
                $ip = $exp_ip[0];
            }
        } else if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } else {
            $ip = getenv('REMOTE_ADDR');
        }
    }
    return $ip;
}

/*
 * getUserIP() és getrealip() IP lekérése a látogatótól vége * 
 */



/** 
* Checks date if matches given format and validity of the date. 
* Examples: 
* <code> 
* is_date('22.22.2222', 'mm.dd.yyyy'); // returns false 
* is_date('11/30/2008', 'mm/dd/yyyy'); // returns true 
* is_date('30-01-2008', 'dd-mm-yyyy'); // returns true 
* is_date('2008 01 30', 'yyyy mm dd'); // returns true 
* </code> 
* @param string $value the variable being evaluated. 
* @param string $format Format of the date. Any combination of <i>mm<i>, <i>dd<i>, <i>yyyy<i> 
* with single character separator between. 
*/ 

function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}

function is_valid_date($value, $format = 'dd.mm.yyyy'){ 
    if(strlen($value) >= 6 && strlen($format) == 10){ 
        
        // find separator. Remove all other characters from $format 
        $separator_only = str_replace(array('m','d','y'),'', $format); 
        $separator = $separator_only[0]; // separator is first character 
        
        if($separator && strlen($separator_only) == 2){ 
            // make regex 
            $regexp = str_replace('mm', '(0?[1-9]|1[0-2])', $format); 
            $regexp = str_replace('dd', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp); 
            $regexp = str_replace('yyyy', '(19|20)?[0-9][0-9]', $regexp); 
            $regexp = str_replace($separator, "\\" . $separator, $regexp); 
            if($regexp != $value && preg_match('/'.$regexp.'\z/', $value)){ 

                // check date 
                $arr=explode($separator,$value); 
                $day=$arr[0]; 
                $month=$arr[1]; 
                $year=$arr[2]; 
                if(@checkdate($month, $day, $year)) 
                    return true; 
            } 
        } 
    } 
    return false; 
} 
