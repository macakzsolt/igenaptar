<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    
    <head><!-- Created by Artisteer v4.2.0.60623 -->
        <meta charset="utf-8">
        <title>API - fejlesztőknek</title>
        <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">

        <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <link rel="stylesheet" href="style.css" media="screen">
        <!--[if lte IE 7]><link rel="stylesheet" href="style.ie7.css" media="screen" /><![endif]-->
        <link rel="stylesheet" href="style.responsive.css" media="all">

        <script type="text/javascript" src="scripts/shCore.js"></script>
        <script type="text/javascript" src="scripts/shBrushJScript.js"></script>
        <script type="text/javascript" src="scripts/shBrushPhp.js"></script>
        <link type="text/css" rel="stylesheet" href="styles/shCoreDefault.css"/>
        <script type="text/javascript">SyntaxHighlighter.all();</script>



        <script src="jquery.js"></script>
        <script src="script.js"></script>
        <script src="script.responsive.js"></script>
        <meta name="keywords" content="NapiIge ima liturgia">

        <script type="text/javascript">
//            var my_location = window.location.toString().match(/\/\/[^\/]+\/([^\.]+)/)[1];
//
//            window.location.href = "" + my_location;

        </script>


    </head>


    <style type="text/css">

        .syntaxhighlighter { 
            overflow-y: hidden !important; 
            overflow-x: auto !important; 
        }

        body .syntaxhighlighter .line {
            white-space: pre-wrap !important;
        }

        .syntaxhighlighter .toolbar {
            background: none repeat scroll 0 0 #6CE26C !important;
            border: medium none !important;
            color: white !important;
            display : none;
        }


    </style>


    <?php
    include 'eszkozok.php';

       
    $url = 'http://'.$_SERVER['SERVER_NAME'].'/api/json/napiige.json';
    
   
    $content = file_get_contents($url);
    $json = json_decode($content, true);
    ?>
    <body>



        <div id="main">


            <div class="sheet clearfix">
                <header class="header">

                    <div class="shapes">

                    </div>

                    <h1 class="headline">
                        <a href="http://<?php echo $_SERVER['SERVER_NAME']?>">Napi Ige</a>
                    </h1>
                    <h2 class="slogan">A Szentmise olvasmányai és a napi zsolozsma</h2>

                    <div id="summary"></div>



                    <nav class="nav">
                        <ul class="hmenu"><li><a href="http://<?php echo $_SERVER['SERVER_NAME']?>" class="">Igenaptár</a></li><li><a href="fejlesztoknek.php" class="active">API</a></li></ul> 
                    </nav>


                </header>
                <div class="layout-wrapper">
                    <div class="content-layout">
                        <div class="content-layout-row">
                            <div class="layout-cell sidebar1">
                                <div class="block clearfix">
                                    <div class="blockheader">
                                        <h3 class="t">JSON oldalak</h3>
                                    </div>
                                    <div class="blockcontent">
                                        <p>
                                            <a href="http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/">Mai nap</a><br>
                                            <a href="http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/2016-12-25">Egyéb nap</a><br>
                     
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="layout-cell content">
                                <article class="post article">
                                    <h2 class="postheader">API - célja és használat</h2>

                                    <div class="postcontent postcontent-0 clearfix">

                                        Ez az API sok sebből is v&eacute;rzik. Egyelőre nem
                                        tal&aacute;ltam
                                        megold&aacute;st arra, <br>
                                        hogy ezeket az adatokat valamilyen egys&eacute;ges
                                        fel&uuml;letről vagy helyről szerezzem be. <br>
                                        A j&ouml;vőbeli &eacute;s a m&aacute;r megl&eacute;vő
                                        alkalmaz&aacute;sok l&eacute;trehoz&aacute;s&aacute;hoz
                                        egy egys&eacute;ges fel&uuml;letet szeretn&eacute;k
                                        biztos&iacute;tani.<br>
                                        PHP, Android &eacute;s Windows projekteken is dolgozom, ebben a
                                        t&eacute;m&aacute;ban, csak mert szakmailag
                                        &eacute;rdekel.&nbsp; <br>
                                        A nem SQL alap&uacute; adatok begyűjt&eacute;se,
                                        feldolgoz&aacute;sa, rendszerez&eacute;se &eacute;s
                                        megjelen&iacute;t&eacute;se volt a c&eacute;lom.<br>
                                        <br>
                                        Ez nem egy professzion&aacute;lis
                                        szolg&aacute;ltat&aacute;s, alapvetően saj&aacute;t
                                        sz&aacute;nd&eacute;kok
                                        megval&oacute;s&iacute;t&aacute;s&aacute;ra
                                        k&eacute;sz&uuml;lt,<br>
                                        de ha felkeltette &eacute;rdeklőd&eacute;sedet, akkor nyugodtan
                                        haszn&aacute;ld. <br>
                                        <br>
                                        &Eacute;p&iacute;tsd be honlapodba vagy k&eacute;rd le az
                                        adatokat az alkalmaz&aacute;sodb&oacute;l.<br>

                                        <br>
                                        <b>Példák:</b>
                                        <?php
                                        if ($json['liturgy']['gospel'] !== "") {

                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>" . $json['liturgy']['gospel'] . "</a> - az evangélium</p>");
                                        }
                                        ?>
                                        </br>
                                        <b>PHP ben:</b>
                                        <pre class="brush: php" class="syntaxhighlight" title="Példa:">
  
                                        <&zwnj;?php
                                        $url = 'http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/napiige.json';
                                        $content = file_get_contents($url);
                                        $json = json_decode($content, true);
                                        
                                        if ($json['liturgy']['gospel'] !== "") {

                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>
                                                    " . $json['liturgy']['gospel'] . "</a></p>");
                                        }
                                        ?>
    
                                        </pre>
                                        Tesztelve: 2016.12.02.
                                        </br></br>
                                        <b>Drupal pl. blokban amenyiben engedélyzett a PHP futtatás</b>
                                        <pre class="brush: php;" title="Példa:">
                                            <&zwnj;?php
                                            $url = 'http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/napiige.json';
                                            $request = drupal_http_request($url);
                                            $json = drupal_json_decode($request->data);

                                            if ($json['liturgy']['gospel'] !== "") {

                                                print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>
                                                        " . $json['liturgy']['gospel'] . "</a></p>");
                                            }
                                            ?>
                                        </pre>
                                        Vagy eseleg <a href="https://drupal.psu.edu/blog/post/render-html-json-object-custom-drupal-module">megvalósítani modulban pl.</a>
                                        </br>Tesztelve: 2016.12.02.
                                        </br></br>


                                        <b>Wordpress amenyiben az Insert PHP modul telepítve és engedéjezve van</b>
                                        <pre class="brush: php;" title="Példa:">
                                        [insert_php]

                                        $url = 'http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/napiige.json';


                                        $content = file_get_contents($url);
                                        $json = json_decode($content, true);

                                        if ($json['liturgy']['gospel'] !== "") {

                                        print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>" . $json['liturgy']['gospel'] . "</a> - az evangélium</p>");
                                        }

                                        [/insert_php]
                                        </pre>
                                        Tesztelve: 2016.12.02.
                                        </br></br>

                                        <b>Szentmise olvasmányai</b>                                     
                                        <?php
                                        print ("<p>" . $json['liturgy']['title'] . "</p>");



                                        if ($json['liturgy']['readingone'] !== "") {
                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingone'] . "</a></p>");
                                        }

                                        if ($json['liturgy']['readingtwo'] !== "") {
                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingtwo'] . "</a></p>");
                                        }

                                        if ($json['liturgy']['psalm'] !== "") {
                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "#zsoltar'>" . $json['liturgy']['psalm'] . "</a></p>");
                                        }

                                        if ($json['liturgy']['gospel'] !== "") {
                                            if ($json['liturgy']['gospel_acclamation'] !== "") {
                                                $gospel_acclamation = "</br>" . $json['liturgy']['gospel_acclamation'];
                                            } else
                                                $gospel_acclamation = "";

                                            if ($json['liturgy']['alleluja'] !== "") {
                                                $alleluja = "</br>" . $json['liturgy']['alleluja'] . "";
                                            } else {
                                                $alleluja = "";
                                            }

                                            print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>" . $json['liturgy']['gospel'] . "</a>" . $gospel_acclamation . str_replace(" – ", " - ", $alleluja) . "</p>");
                                        }
                                        ?>

                                        <pre class="brush: php;" title="Példa:">
                                            <&zwnj;?php
                                            print ("<p>" . $json['liturgy']['title'] . "</p>");



                                            if ($json['liturgy']['readingone'] !== "") {
                                                print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingone'] . "</a></p>");
                                            }

                                            if ($json['liturgy']['readingtwo'] !== "") {
                                                print ("<p><a href='" . $json['source']['liturgy_url'] . "'>" . $json['liturgy']['readingtwo'] . "</a></p>");
                                            }

                                            if ($json['liturgy']['psalm'] !== "") {
                                                print ("<p><a href='" . $json['source']['liturgy_url'] . "#zsoltar'>" . $json['liturgy']['psalm'] . "</a></p>");
                                            }

                                            if ($json['liturgy']['gospel'] !== "") {
                                                if ($json['liturgy']['gospel_acclamation'] !== "") {
                                                    $gospel_acclamation = "</br>" . $json['liturgy']['gospel_acclamation'];
                                                } else
                                                    $gospel_acclamation = "";

                                                if ($json['liturgy']['alleluja'] !== "") {
                                                    $alleluja = "</br>" . $json['liturgy']['alleluja'] . "";
                                                } else {
                                                    $alleluja = "";
                                                }

                                                print ("<p><a href='" . $json['source']['liturgy_url'] . "#evangelium'>" . $json['liturgy']['gospel'] . "</a>" . $gospel_acclamation . str_replace(" – ", " - ", $alleluja) . "</p>");
                                            }
                                            ?>
                                        </pre>
                                        Tesztelve: 2016.12.02.
                                        </br></br>

                                        <b>Zsolozsma</b>

                                        <?php
                                        $breviary_url = $json['source']["breviary_url"];

                                        if ($json['breviary'][0]['titleone'] !== "") {

                                            $communia = "";
                                            $color = "";
                                            echo "<ul>";
                                            for ($i = 0; $i < count($json['breviary']); $i++) {

                                                if ($json['breviary'][$i]["titleone"] !== "") {

                                                    $titleone = $json['breviary'][$i]["titleone"];
                                                }

                                                if ($json['breviary'][$i]["communia"] !== "") {

                                                    $communia = $json['breviary'][$i]["communia"] . "</BR>";
                                                }

                                                if ($json['breviary'][$i]["titletwo"] !== "") {

                                                    $titletwo = $json['breviary'][$i]["titletwo"] . "</BR>";
                                                }
                                                if ($json['breviary'][$i]['color'] !== "") {

                                                    $color = "" . $json['breviary'][$i]['color'] . "</BR>";
                                                }

                                                echo "<li><a href='" . $breviary_url . "'>" . $titleone . "</a></BR>"
                                                . $communia
                                                . $titletwo
                                                . $color
                                                . "</li>";
                                            }
                                            echo "</ul>";
                                        } else
                                            echo $json['breviary']['titleone'];
                                        ?>
                                        <pre class="brush: php;" title="Példa:">
                                            <&zwnj;?php
                                            $breviary_url = $json['source']["breviary_url"];

                                            if ($json['breviary'][0]['titleone'] !== "") {


                                                echo "<ul>";
                                                for ($i = 0; $i < count($json['breviary']); $i++) {

                                                    if ($json['breviary'][$i]["titleone"] !== "") {

                                                        $titleone = $json['breviary'][$i]["titleone"];
                                                    }

                                                    if ($json['breviary'][$i]["communia"] !== "") {

                                                        $communia = $json['breviary'][$i]["communia"] . "</BR>";
                                                    }

                                                    if ($json['breviary'][$i]["titletwo"] !== "") {

                                                        $titletwo = $json['breviary'][$i]["titletwo"] . "</BR>";
                                                    }
                                                    if ($json['breviary'][$i]['color'] !== "") {

                                                        $color = "" . $json['breviary'][$i]['color'] . "";
                                                    }

                                                    echo "<li><a href='" . $breviary_url . "'>" . $titleone . "</a></BR>"
                                                    . $communia
                                                    . $titletwo
                                                    . $color
                                                    . "</li>";
                                                }   
                                                echo "</ul>";
                                            } else
                                                echo $json['breviary']['titleone'];
                                            ?>  

                                        </pre>
                                        Tesztelve: 2016.12.02.
                                        </br></br>


                                        <b>A nap szentje - emléknap</b></br>

                                        <?php
                                        if ($json['dailysaint']['name'] !== "") {
                                            $dailysaint = "<a style='text-decoration: underline;' href='" . $json['dailysaint']['url'] . "'><b>" . $json['dailysaint']['name'] . "</b></a></br>";
                                            echo $dailysaint;
                                        }
                                        ?>
                                        <pre class="brush: php;" title="Példa:">
                                            <&zwnj;?php
                                            if ($json['dailysaint']['name'] !== "") {
                                                $dailysaint = "<a style='text-decoration: underline;' href='" . $json['dailysaint']['url'] . "'><b>" . $json['dailysaint']['name'] . "</b></a></br>";
                                                echo $dailysaint;
                                            }
                                            ?>

                                        </pre>
                                        Tesztelve: 2016.12.02.
                                        </br></br>




                                        <p>
                                            A mai nap json k&oacute;d el&eacute;r&eacute;se: &nbsp;<a
                                                href="http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/">http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/napiige.json</a><br>
                                            Egy&eacute;b nap el&eacute;r&eacute;se: pl. &nbsp;<a
                                                href="http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/2017-02-25">http://<?php echo $_SERVER['SERVER_NAME']?>/api/json/2017-02-25</a>


                                        </p>



                                </article>
                            </div>
                        </div>
                    </div>
                </div><footer class="footer">
                    <div class="">
                        <div class="">
                            <h5 class="">Adatokat szolgáltató forrás oldalak</h5>
                        </div>
                        <div class="">

                            <a href="http://igenaptar.katolikus.hu/"><h5>Igenaptár</h5></a> <a href="http://zsolozsma.katolikus.hu/"><h5>Zsolozsma</h5></a> <a href="http://szentiras.hu/"><h5>Szentiras.hu</h5></a>

                        </div>
                    </div>
                    <p>Copyright © 2016.</p>
                </footer>

            </div>
        </div>


    </body></html>